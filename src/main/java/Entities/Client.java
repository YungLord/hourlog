package Entities;

public class Client {

	private int id; 
	private String name;
	private String manager;
	private String position;
	
	


 	public Client() {
		
	}
	

	public Client(int id, String name, String manager, String position) {
		super();
		this.id = id;
		this.name = name;
		this.manager = manager;
		this.position = position;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getManager() {
		return manager;
	}


	public void setManager(String manager) {
		this.manager = manager;
	}


	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", manager=" + manager + ", position=" + position + "]";
	}

	
	
}
