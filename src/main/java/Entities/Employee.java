package Entities;


public class Employee {

	private int id;
	private String address;
	private String firstName;
	private String lastName;
	private String hashPassword; 
	private int userId;
	
	
	public Employee()
	{
		this.id = 1;
		this.address = "yah";
		this.firstName = "leroy";
		this.lastName = "Russel";
		this.hashPassword = "@#$%^&vfjsbknsdsnksdf";
		this.userId = 1234567;
	}


	public Employee(int id, String address, String firstName, String lastName, String hashPassword, int userId) {
		super();
		this.id = id;
		this.address = address;
		this.firstName = firstName;
		this.lastName = lastName;
		this.hashPassword = hashPassword;
		this.userId = userId;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getHashPassword() {
		return hashPassword;
	}


	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	@Override
	public String toString() {
		return "Employee [id=" + id + ", address=" + address + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", hashPassword=" + hashPassword + ", userId=" + userId + "]";
	}
	
	
	
	
	

	

}
