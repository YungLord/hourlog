package Entities;




public class Record {

	private int id;
	private String title;
	private String client;
	private String date;
	private String startTime;
	private String endTime;
	private String comment;
		
	public Record() {
		this.id = 1;
		this.title = "SLB";
		this.client = "Solid";
		this.date = "May 1, 2019";
		this.startTime = "9:00 am";
		this.endTime = "5:00 pm";
		this.comment = "car";	
	}

	public Record(int id, String title, String client, String date, String startTime, String endTime, String comment) {
		super();
		this.id = id;
		this.title = title;
		this.client = client;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", title=" + title + ", client=" + client + ", date=" + date + ", startTime="
				+ startTime + ", endTime=" + endTime + ", comment=" + comment + "]";
	}

}
