package Database;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Entities.Client;

public class ClientDb extends SqlSource<Client> {

	Logger logger = LogManager.getLogger(ClientDb.class);	
	private static final String TABLE_NAME = "yung_Client";
	                                                                    
	
	@Override
	protected void initSQLDatabase() {
		try {
			statement = connect.createStatement();
			if (statement
					.execute("CREATE TABLE if not exists "+TABLE_NAME+
							"(id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(50),manager VARCHAR(50), position  VARCHAR(50) )"))
			{
				logger.debug("Client table created");
			} 
			else
			{
				logger.debug("Client table does not need to be created");
			}
			logger.debug("Client table exists");
			
			} catch (SQLException e)
			{
				e.printStackTrace();
				logger.error("Unable to initialize SQL database, Client Table not created ", e);
			}
	}

	@Override
	public int add(Client item) 
	{
		try{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 
			connect = DriverManager.getConnection(url);
			String query = "INSERT INTO "+TABLE_NAME
					       + "(name,manager,position)  VALUES (?,?,?)";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setString(1, item.getName());
			ps.setString(2, item.getManager());
			ps.setString(3, item.getPosition());	
			return ps.executeUpdate();
			
    	}catch(SQLException e){
    		e.printStackTrace();
			logger.error("Unable to add Client",e);
			return 0;

		}
		
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		
	}
	
	@Override
	public List<Client> selectAll() { 
		List<Client> items = new ArrayList<Client>();
		try {
//			String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			Statement statement = connect.createStatement();
			String sql = "SELECT id, name, manager, position from "+TABLE_NAME;
			ResultSet rs = statement.executeQuery(sql);
			if(rs != null) 
			{
				while(rs.next()) 
				{				
					Client Client = new Client();						
					Client.setId(rs.getInt("id"));
					Client.setName(rs.getString("name"));
					Client.setManager(rs.getString("manager"));
					Client.setPosition(rs.getString("position"));
					items.add(Client);					
				}
			}
			return items;
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to select elements in Client database",e);
			return null;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	
	@Override
	public Client get(int id) {
		try
		{
//			String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true"; 
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			Statement stat;
			stat = connect.createStatement();			
			String query = "Select * from " +TABLE_NAME+ " WHERE id = "+id;   
			ResultSet rs= stat.executeQuery(query);
			if(rs != null)
			{
				while(rs.next())
				{					
					Client Client = new Client();
					Client.setId(rs.getInt("id"));
					Client.setName(rs.getString("name"));
					Client.setManager(rs.getString("manager"));
					Client.setPosition(rs.getString("position"));		
					return Client;
				}								
			}				
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			logger.error("Unable to retrieve Client recorf=d",e);
		}
		

		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		return null;
	}

	
	@Override
	public int update(Client item, int id) 
	{		
		try 
		{	
//			String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true"; 
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			String query = " UPDATE " +TABLE_NAME+ " SET name = ?, manager = ?, position = ? " +
					   " WHERE id = ?";
			PreparedStatement ps;		
			ps = connect.prepareStatement(query);				
			ps.setString(1, item.getName());
			ps.setString(2, item.getManager());
			ps.setString(3, item.getPosition());	
			ps.setInt(4,id);
			return ps.executeUpdate();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to update Client record",e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	

	@Override
	public int delete(int id) 
	{
		try 
		{
//			String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true"; 
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id = ?";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setInt(1,id);
			return ps.executeUpdate();
		}
	
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("Unable to delete Client Manager with id "+id,e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	@Override
	public int deleteMultiple(int[] ids)
	{
		try 
		{
//			String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true"; 
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			String groupedIds = Arrays.toString(ids).replace("[","").replace("]","");
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id in ("+groupedIds+")";
			PreparedStatement ps = connect.prepareStatement(query);			
			return ps.executeUpdate();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to delete multiple",e);
			return 0;
		}
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	

}
