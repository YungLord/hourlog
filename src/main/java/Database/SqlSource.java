package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public abstract class SqlSource <T>{


	protected Connection connect = null; // connection object to database
	protected Statement statement = null; // use to execute sql statements
	protected ResultSet result = null; // used to collect result data from 	// database
					

	private static final String DRIVER = "org.sqlite.JDBC"; 
//	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	
	private static Logger logger = LogManager.getLogger(SqlSource.class);
	
	
	@SuppressWarnings("deprecation")
	public SqlSource() 
	{
		try
		{
			logger.info("Attempting to connect to database");
			Class.forName(DRIVER).newInstance();
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			//String url = "jdbc:mysql://localhost:3306/TimeSheet?useSSL=false&createDatabaseIfNotExist=true";
			connect = DriverManager.getConnection(url);
			
			
			initSQLDatabase();

			logger.info("Connected to database");
			logger.debug( "Connected to database");

		}		
		catch(SQLException ex)
		{
			logger.error("Could not connect to database",ex);
		}
		catch (ClassNotFoundException ex) 
		{
			logger.error("Failed to load JDBC Driver",ex);
		}// end catch
		catch (NullPointerException ex) 
		{
			logger.error("Could not find database",ex);
		}// end catch
		 catch (InstantiationException e) {
					// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	   abstract protected void initSQLDatabase();
	   
	   abstract public List<T> selectAll();
	   
	   abstract public T get(int id);
	   
	   abstract public int update(T item, int id);
	   
	   abstract public int delete(int id);
	   
	   abstract public int deleteMultiple(int[] ids);
	   
	   abstract public int add(T item);
	   

	   


}
