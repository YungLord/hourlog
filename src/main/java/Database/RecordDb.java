package Database;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Entities.Record;



public class RecordDb extends SqlSource<Record> {

	Logger logger = LogManager.getLogger(RecordDb.class);	
	private static final String TABLE_NAME = "yung_Record";
	                                                                    
	
	@Override
	protected void initSQLDatabase() {
		try {
			statement = connect.createStatement();
			if (statement
					.execute("CREATE TABLE if not exists "+TABLE_NAME+
							" (id INTEGER PRIMARY KEY AUTOINCREMENT,title VARCHAR(50), client VARCHAR(50),date VARCHAR(50) , startTime VARCHAR(50), endTime VARCHAR(50), comment VARCHAR(50) )"))
			{
				logger.debug("Record table created");
			} 
			else
			{
				logger.debug("Record table does not need to be created");
			}
			logger.debug("Record table exists");
			
			} catch (SQLException e)
			{
				e.printStackTrace();
				logger.error("Unable to initialize SQL database, Record Table not created ", e);
			}
	}

	@Override
	public int add(Record item) 
	{
		try{
			
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";	
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			String query = "INSERT INTO "+TABLE_NAME
					       + "(title,client,date,startTime,endTime,comment)  VALUES (?,?,?,?,?,?)";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setString(1, item.getTitle());
			ps.setString(2, item.getClient());
			ps.setString(3,item.getDate());
			ps.setString(4,item.getStartTime());
			ps.setString(5,item.getEndTime());
			ps.setString(6,item.getComment());	
			return ps.executeUpdate();
			
    	}catch(SQLException e){
    		e.printStackTrace();
			logger.error("Unable to add Record",e);
			return 0;
		}			
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		
	}
	
	@Override
	public List<Record> selectAll() { 
		List<Record> items = new ArrayList<Record>();
		try {
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			Statement statement = connect.createStatement();
			String sql = "SELECT id, title, client, date, startTime, endTime,comment from "+TABLE_NAME;
			ResultSet rs = statement.executeQuery(sql);
			if(rs != null) 
			{
				while(rs.next()) 
				{				
					Record Record = new Record();						
					Record.setId(rs.getInt("id"));
					Record.setTitle(rs.getString("title"));
					Record.setClient(rs.getString("client"));
					Record.setDate(rs.getString("date"));					
					Record.setStartTime(rs.getString("startTime"));
					Record.setEndTime(rs.getString("endTime"));
					Record.setComment(rs.getString("comment"));
					items.add(Record);					
				}
			}
			return items;
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to select elements in Record database",e);
			return null;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	
	@Override
	public Record get(int id) {
		try
		{
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			Statement stat;
			stat = connect.createStatement();			
			String query = "Select * from " +TABLE_NAME+ " WHERE id = "+id;   
			ResultSet rs= stat.executeQuery(query);
			if(rs != null)
			{
				while(rs.next())
				{					
					Record Record = new Record();
					Record.setId(rs.getInt("id"));
					Record.setTitle(rs.getString("title"));
					Record.setClient(rs.getString("client"));
					Record.setDate(rs.getString("date"));					
					Record.setStartTime(rs.getString("startTime"));
					Record.setEndTime(rs.getString("endTime"));
					Record.setComment(rs.getString("comment"));
					return Record;
				}								
			}				
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			logger.error("Unable to retrieve Record recorf=d",e);
		}
		

		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		return null;
	}

	
	@Override
	public int update(Record item, int id) 
	{		
		try 
		{	
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			String query = " UPDATE " +TABLE_NAME+ " SET title = ?, client= ? ,date = ?, startTime = ?,endTime =?, comment = ?  " +
					   " WHERE id = ?";
			PreparedStatement ps;		
			ps = connect.prepareStatement(query);				
			ps.setString(1, item.getTitle());
			ps.setString(2, item.getClient());
			ps.setString(3,item.getDate());
			ps.setString(4,item.getStartTime());
			ps.setString(5,item.getEndTime());
			ps.setString(6,item.getComment());
			ps.setInt(7,id);			
			return ps.executeUpdate();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to update Record record",e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	

	@Override
	public int delete(int id) 
	{
		try 
		{
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id = ?";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setInt(1,id);
			return ps.executeUpdate();
		}
	
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("Unable to delete Record Manager with id "+id,e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	@Override
	public int deleteMultiple(int[] ids)
	{
		try 
		{
//			String url = "jdbc:mysql://localhost:3306/Tigr?useSSL=false&createDatabaseIfNotExist=true";
			String url = "jdbc:sqlite:TimeSheet.sqlite";
			connect = DriverManager.getConnection(url);
			
			String groupedIds = Arrays.toString(ids).replace("[","").replace("]","");
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id in ("+groupedIds+")";
			PreparedStatement ps = connect.prepareStatement(query);			
			return ps.executeUpdate();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to delete multiple",e);
			return 0;
		}
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	



}
