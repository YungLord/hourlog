package Database;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Entities.Employee;

public class EmployeeDb extends SqlSource<Employee> {

	Logger logger = LogManager.getLogger(EmployeeDb.class);	
	private static final String TABLE_NAME = "yung_Employee";
	                                                                    
	
	@Override
	protected void initSQLDatabase() {
		try {
			statement = connect.createStatement();
			if (statement
					.execute("CREATE TABLE if not exists "+TABLE_NAME+
							"(id INTEGER PRIMARY KEY AUTOINCREMENT,address VARCHAR(50), firstName VARCHAR(50), lastName VARCHAR(50), hashPassword VARCHAR(50), userId INTEGER )"))
			{
				logger.debug("Employee table created");
			} 
			else
			{
				logger.debug("Employee table does not need to be created");
			}
			logger.debug("Employee table exists");
			
			} catch (SQLException e) 
			{
				e.printStackTrace();
				logger.error("Unable to initialize SQL database, Employee Table not created ", e);
			}
	}

	@Override
	public int add(Employee item) 
	{
		try{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			String query = "INSERT INTO "+TABLE_NAME
					       + "(address,firstName, lastName, hashPassword, userId )  VALUES (?,?,?,?,?)";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setString(1, item.getAddress());
			ps.setString(2, item.getFirstName());
			ps.setString(3, item.getLastName());
			ps.setString(4, item.getHashPassword());
			ps.setInt(5, item.getUserId());						
			return ps.executeUpdate();
					
    	}catch(SQLException e){
    		e.printStackTrace();
			logger.error("Unable to add Employee",e);
			return 0;

		}
		
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		
	}
	
	@Override
	public List<Employee> selectAll() {
		List<Employee> items = new ArrayList<Employee>();
		try {
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	 	
			connect = DriverManager.getConnection(url);
			
			Statement statement = connect.createStatement();
			String sql = "SELECT id, address,firstName, lastName, hashPassword,userId from "+TABLE_NAME;
			ResultSet rs = statement.executeQuery(sql);
			if(rs != null) 
			{
				while(rs.next()) 
				{				
					Employee Employee = new Employee();						
					Employee.setId(rs.getInt("id"));
					Employee.setAddress(rs.getString("address"));
					Employee.setFirstName(rs.getString("firstName"));
					Employee.setLastName(rs.getString("lastName"));
					Employee.setHashPassword(rs.getString("hashPassword"));
					Employee.setUserId(rs.getInt("userId"));
					items.add(Employee);					
				}
			}
			return items;
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to select elements in Employee database",e);
			return null;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	
	@Override
	public Employee get(int id) {
		try
		{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			Statement stat;
			stat = connect.createStatement();			
			String query = "Select * from " +TABLE_NAME+ " WHERE id = "+id;   
			ResultSet rs= stat.executeQuery(query);
			if(rs != null)
			{
				while(rs.next())
				{					
					Employee Employee = new Employee();
					Employee.setId(rs.getInt("id"));
					Employee.setAddress(rs.getString("address"));
					Employee.setFirstName(rs.getString("firstName"));
					Employee.setLastName(rs.getString("lastName"));
					Employee.setHashPassword(rs.getString("hashPassword"));
					Employee.setUserId(rs.getInt("userId"));		
					return Employee;
				}								
			}				
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			logger.error("Unable to retrieve Employee recorf=d",e);
		}
		

		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		return null;
	}

	
	@Override
	public int update(Employee item, int id) 
	{		
		try 
		{	
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			String query = " UPDATE " +TABLE_NAME+ " SET address = ?, firstName = ?, lastName = ?, hashPassword = ?, Us " +
					   " WHERE id = ?";
			PreparedStatement ps;		
			ps = connect.prepareStatement(query);				
			ps.setString(1, item.getAddress());
			ps.setString(2, item.getFirstName());
			ps.setString(3, item.getLastName());
			ps.setString(4, item.getHashPassword());
			ps.setInt(5, item.getUserId());
			ps.setInt(6,id);
			return ps.executeUpdate();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to update Employee record",e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	

	@Override
	public int delete(int id) 
	{
		try 
		{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id = ?";
			PreparedStatement ps = connect.prepareStatement(query);			
			ps.setInt(1,id);
			return ps.executeUpdate();
		}
	
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("Unable to delete Employee Manager with id "+id,e);
			return 0;
		}
		
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}

	@Override
	public int deleteMultiple(int[] ids)
	{
		try 
		{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			String groupedIds = Arrays.toString(ids).replace("[","").replace("]","");
			String query = "DELETE FROM "+TABLE_NAME+ " WHERE id in ("+groupedIds+")";
			PreparedStatement ps = connect.prepareStatement(query);			
			return ps.executeUpdate();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("unable to delete multiple",e);
			return 0;
		}
		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
	}
	
	
	
	public Employee getByUserId(int userId) {
		try
		{
			String url = "jdbc:sqlite:TimeSheet.sqlite"; 	
			connect = DriverManager.getConnection(url);
			
			Statement stat;
			stat = connect.createStatement();			
			String query = "Select * from " +TABLE_NAME+ " WHERE userId = "+userId;   
			ResultSet rs= stat.executeQuery(query);
			if(rs != null)
			{
				while(rs.next())
				{					
					Employee Employee = new Employee();
					Employee.setId(rs.getInt("id"));
					Employee.setAddress(rs.getString("address"));
					Employee.setFirstName(rs.getString("firstName"));
					Employee.setLastName(rs.getString("lastName"));
					Employee.setHashPassword(rs.getString("hashPassword"));
					Employee.setUserId(rs.getInt("userId"));		
					return Employee;
				}								
			}				
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			logger.error("Unable to retrieve Employee record",e);
		}
		

		finally {			
			  if (connect != null) {
				    try {
				      connect.close(); // <-- This is important				      
				      logger.info("connection closed");
				    } catch (SQLException ex) 
					{
						logger.error("Could not close connection to database",ex);
					}
			  }
		}
		return null;
	}


}
