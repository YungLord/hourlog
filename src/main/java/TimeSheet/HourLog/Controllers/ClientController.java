package TimeSheet.HourLog.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import Database.ClientDb;
import Entities.Client;
import exceptions.ResourceNotFoundException;


@RestController
@RequestMapping("/api/client")
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {

	private ClientDb clientdb = new ClientDb();
	Logger logger = LogManager.getLogger(ClientController.class);
	
	@GetMapping("/all")
	public List<Client> getAll() {
		return clientdb.selectAll(); 
	}
	
	@PostMapping("/add")
	public Client create(@Valid @RequestBody Client data) {
		clientdb.add(data);
		return data;
	}
	
	/*@PostMapping("/{id}")
	public Client create(@PathVariable(value = "id") int id,@Valid @RequestBody Client data) {
		clientdb.add(data);
		return data;
	}
	*/
	@GetMapping("/{id}")
	public Client get(@PathVariable(value="id")  int id) {
		
		
		
		Client item = new Client();	
		
		item = clientdb.get(id);	 	
		try 
		{		
			if (item != null )	
			{
				logger.info("item exits");				
			}	
			return item;
			
		}
	catch(ResourceNotFoundException e ) 
		{
		e.printStackTrace();
		return null;
		}
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Client update(@PathVariable(value = "id") int id,@Valid @RequestBody Client details) {
	
		

		try
		{		
			Client current = clientdb.get(id);
			clientdb.update(details, id);			
			return current;
		}
		catch(ResourceNotFoundException e ) 
			{
			e.printStackTrace();
			return null;
			}

	}
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {

	Client client = clientdb.get(id);	        
    try
	{		
		 if (client != null)
		 {
			 clientdb.delete(id);
		 }
		 return ResponseEntity.ok().build();
	}
	catch(ResourceNotFoundException e )
	{
		e.printStackTrace();
		return null;
	}

}
}
