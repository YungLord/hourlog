
package TimeSheet.HourLog.Controllers;
 

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Database.EmployeeDb;
import Entities.Employee;
import exceptions.ResourceNotFoundException;


@RestController
@RequestMapping("/api/employee")
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {
	private EmployeeDb employeedb = new EmployeeDb();
	Logger logger = LogManager.getLogger(EmployeeController.class);
	
	@GetMapping("/all")
	public List<Employee> getAll() {
		return employeedb.selectAll(); 
	}
	
	@PostMapping("/add")
	public Employee create(@Valid @RequestBody Employee data) {
		employeedb.add(data);
		return data;
	}
	

	@GetMapping("/{id}")
	public Employee get(@PathVariable(value="id")  int id) {
		
		
		
		Employee item = new Employee();	
		
		item = employeedb.get(id);	 	
		try 
		{		
			if (item != null )	
			{
				logger.info("item exits");				
			}	
			return item;
			
		}
	catch(ResourceNotFoundException e ) 
		{
		e.printStackTrace();
		return null;
		}
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Employee update(@PathVariable(value = "id") int id,@Valid @RequestBody Employee details) {
	
		

		try
		{		
			Employee current = employeedb.get(id);
			employeedb.update(details, id);			
			return current;
		}
		catch(ResourceNotFoundException e ) 
			{
			e.printStackTrace();
			return null;
			}

	}
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {

	Employee employee = employeedb.get(id);	        
    try
	{		
		 if (employee != null)
		 {
			 employeedb.delete(id);
		 }
		 return ResponseEntity.ok().build();
	}
	catch(ResourceNotFoundException e )
	{
		e.printStackTrace();
		return null;
	}

}
	
	
	
	@GetMapping("/get/{userId}")
	public Employee getByUserId(@PathVariable(value="userId")  int userId) {
		
		
		
		Employee item = new Employee();	
		
		item = employeedb.getByUserId(userId);	 	
		try 
		{		
			if (item != null )	
			{
				logger.info("item exits");				
			}	
			return item;
			
		}
	catch(ResourceNotFoundException e ) 
		{
		e.printStackTrace();
		return null;
		}
	}
	
	
	
	@PostMapping("/verify")
	public int Verify(@Valid @RequestBody Employee data) {
	

		EmployeeDb db = new EmployeeDb();
		Employee record = new Employee();
		int userId = data.getUserId();
		String hash ;
		String clientHash;
		int cmp;
		record = db.getByUserId(userId);
		if (record !=  null)
		{
			hash = record.getHashPassword();
			clientHash =data.getHashPassword();
			logger.info(hash);
			logger.info( clientHash);
			cmp = hash.compareTo(clientHash);
			if( cmp == 0)
			{
				return 1;			
			}	
			else
			{
				
				return 0;
			}
		}
		else
		{
			return 404;
		}
		
	
	
	
}
	
}
	


