package TimeSheet.HourLog.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import Database.RecordDb;
import Entities.Record;
import exceptions.ResourceNotFoundException;


@RestController
@RequestMapping("/api/record")
@CrossOrigin(origins = "http://localhost:4200")
public class RecordController {


	private RecordDb recorddb = new RecordDb();
	Logger logger = LogManager.getLogger(RecordController.class);
	
	@GetMapping("/all")
	public List<Record> getAll() {
		return recorddb.selectAll(); 
	}
	
	@PostMapping("/add")
	public Record create(@Valid @RequestBody Record data) {
		recorddb.add(data);
		return data;
	}
	
	/*@PostMapping("/{id}")
	public Record create(@PathVariable(value = "id") int id,@Valid @RequestBody Record data) {
		recorddb.add(data);
		return data;
	}
	*/
	@GetMapping("/{id}")
	public Record get(@PathVariable(value="id")  int id) {
		
		
		
		Record item = new Record();	
		
		item = recorddb.get(id);	 	
		try 
		{		
			if (item != null )	
			{
				logger.info("item exits");				
			}	
			return item;
			
		}
	catch(ResourceNotFoundException e ) 
		{
		e.printStackTrace();
		return null;
		}
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Record update(@PathVariable(value = "id") int id,@Valid @RequestBody Record details) {
	
		

		try
		{		
			Record current = recorddb.get(id);
			recorddb.update(details, id);			
			return current;
		}
		catch(ResourceNotFoundException e ) 
			{
			e.printStackTrace();
			return null;
			}

	}
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {

	Record record = recorddb.get(id);	        
    try
	{		
		 if (record != null)
		 {
			 recorddb.delete(id);
		 }
		 return ResponseEntity.ok().build();
	}
	catch(ResourceNotFoundException e )
	{
		e.printStackTrace();
		return null;
	}

}

}