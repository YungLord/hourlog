package TimeSheet.HourLog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HourLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(HourLogApplication.class, args);
	}

}
