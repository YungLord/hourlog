import Entities.Employee;
import Database.EmployeeDb;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class AppSecurity {

	public static void main(String[] args) {
	
		Logger logger = LogManager.getLogger(AppSecurity.class);	
		EmployeeDb db = new EmployeeDb();
		int recordAffected;
		Employee employee = new Employee(1,"Iron Shore","Earlon","Bartley","manhimself",201617);
		
	
		recordAffected = db.add(employee);
		
		if (recordAffected == 1)
		{
			logger.error("Employee added successfully");
		}
		else
		{
			logger.info("Error adding Employee item ");
		}
		
		

		
		List<Employee> result = new ArrayList<Employee>(); 
		result = db.selectAll();
		

		for (Employee s : result)
		{
			System.out.println(s);
		}
		
/*
		int userId = 1;
		Employee item = new Employee();
		item = db.getByUserId(userId);
		System.out.println(item);
		*/
		
		
		
		
	}

}
