import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Database.ClientDb;
import Database.RecordDb;

import Entities.Client;
import Entities.Record;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;
public class Driver {
	public static void main(String args[])
	{
	Logger logger = LogManager.getLogger(Driver.class);	
	
	RecordDb db = new RecordDb();
	//ClientDb db = new ClientDb();
	int recordsAffected = 0;
	
	Record record = new Record(1,"SLB","NHT","March 18 2018","545","1246","another one");
	
	
	recordsAffected = db.add(record);
	
	if (recordsAffected == 1)
	{
		logger.error("Record added successfully");
	}
	else
	{
		logger.info("Error adding Record item ");
	}
	
	

	
	List<Record> result = new ArrayList<Record>(); 
	result = db.selectAll();
	

	for (Record s : result)
	{
		System.out.println(s);
	}
	
	/*
	Client client = new Client(1,"Jamaica Puplic Service","Sam Hunt","Department Head");
	recordsAffected = db.add(client);
	
	if (recordsAffected == 1)
	{
		logger.error("Record added successfully");
	}
	else
	{
		logger.info("Error adding Client item ");
	}
	
	
	List<Client> result = new ArrayList<Client>(); 
	result = db.selectAll();
	

	for (Client s : result)
	{
		System.out.println(s);
	}
	*/
}
	
	
}